import java.util.Scanner;

/**
 * Classe SoloNoble
 * @author SCHWARZ_Camille_S3A
 * @author MILLARDET_Quentin_S3A
 */
public class SoloNoble {

    /**
     * Gestionnaire du tablier
     */
    public Grille grille;

    /**
     * Nombre d'iterations de la fonction recursive
     */
    public static int nb = 0;

    /**
     * Constructeur de la classe SoloNoble
     *
     * @param gr
     *      Gestionnaire du tablier
     */
    public SoloNoble(Grille gr){
        this.grille = gr;
    }

    /**
     * Methode resoudreSoloNoble qui retourne true si aucune solution de resolution de tablier n'est trouve
     *
     * @param billes
     *      Nombre de billes du tablier
     *
     * @return true si aucune solution de resolution de tablier n'est trouve
     */
    public boolean resoudreSoloNoble(int billes){
        boolean echec =  true, ann = false;
        if (billes == 1){
            System.out.print("Solution trouvée\n" + this.grille);
            echec = false;
        }
        else {
            SoloNoble.nb++;
            int i = 0;
            int j = 0;
            Boolean resDep;
            while (echec && i < grille.tailleX()) {
                while (echec && j < grille.tailleY()) {
                    if (grille.caseJeu(i, j)) {
                        if (grille.caseLibre(i, j)) {
                            if (grille.caseBille(i, j - 2) && grille.caseBille(i, j - 1)) {
                                resDep = grille.deplacementBille(i, j - 2, i, j);
                                if (resDep) {
                                    try {
                                        echec = resoudreSoloNoble(grille.nbBille());
                                        if (echec) {
                                            ann = true;
                                            grille.annulation(i, j, i, j - 2);
                                        }
                                    } catch (StackOverflowError e) {
                                        if (!ann) {
                                            System.gc();
                                            grille.annulation(i, j, i, j - 2);
                                        }
                                    }
                                }
                            }

                            if (grille.caseBille(i - 1, j) && grille.caseBille(i - 2, j)) {
                                grille.deplacementBille(i - 2, j, i, j);
                                try {
                                    echec = resoudreSoloNoble(grille.nbBille());
                                    if (echec) {
                                        ann = true;
                                        grille.annulation(i, j, i - 2, j);
                                    }
                                } catch (StackOverflowError e) {
                                    if (!ann) {
                                        System.gc();
                                        grille.annulation(i, j, i - 2, j);
                                    }
                                }
                            }

                            if (grille.caseBille(i, j + 2) && grille.caseBille(i, j + 1)) {
                                grille.deplacementBille(i, j + 2, i, j);
                                try {
                                    echec = resoudreSoloNoble(grille.nbBille());
                                    if (echec) {
                                        ann = true;
                                        grille.annulation(i, j, i, j + 2);
                                    }
                                } catch (StackOverflowError e) {
                                    if (!ann) {
                                        System.gc();
                                        grille.annulation(i, j, i, j + 2);
                                    }
                                }
                            }
                            if (grille.caseBille(i + 1, j) && grille.caseBille(i + 2, j)) {
                                grille.deplacementBille(i + 2, j, i, j);
                                try {
                                    echec = resoudreSoloNoble(grille.nbBille());
                                    if (echec) {
                                        ann = true;
                                        grille.annulation(i, j, i + 2, j);
                                    }
                                } catch (StackOverflowError e) {
                                    if (!ann) {
                                        System.gc();
                                        grille.annulation(i, j, i + 2, j);
                                    }
                                }
                            }

                        }
                    }
                    j++;
                    ann = false;
                }
                i++;
                j = 0;
            }
        }
        return echec;
    }

    /**
     * Methode main qui permet d'executer le programme
     *
     * @param args
     *      Tableau contenant tous les arguments
     *
     * @throws Exception
     */
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        Grille gr;
        String s;
        if(args.length == 0 ){
             gr = new Grille("Aucun Argument propose");
        }
        else {

            gr = new Grille(args[0]);
        }
        SoloNoble solo = new SoloNoble(gr);
        System.out.println("Grille de Depart : ");
        String n = new String();
        char[][] tab = gr.getTablier();
        for(int i=0;i<gr.tailleX();i++){
            for(int j=0;j<gr.tailleY(); j++){
                n += tab[i][j];
            }
            n+="\n";
        }
        System.out.println(n);
        System.out.println("Grille("+gr.tailleX()+","+gr.tailleY()+"). Nombre de billes : " + gr.nbBilleDepart+"\n");
        solo.resoudreSoloNoble(gr.nbBille());
    }
    
}