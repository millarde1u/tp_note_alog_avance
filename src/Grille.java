import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe Grille
 * @author SCHWARZ_Camille_S3A
 * @author MILLARDET_Quentin_S3A
 */
public class Grille {

    /**
     * Tablier sur lequelon travaille
     */
    private char[][] tablier;

    /**
     * Liste des etapes pour resoudre le tablier
     */
    public List<char[][]> liste_tablier;

    /**
     * Nombre de bille dans le tablier au depart
     */
    public int nbBilleDepart;

    /**
     * Constructeur de la classe Grille
     *
     * @param emplacement
     *      Fichier contenant le tablier
     * @throws Exception
     */
    public Grille(String emplacement) throws Exception {
        if (emplacement.equals("Aucun Argument propose")) {
            int l = 7, c = 7;
            this.tablier = new char[l][c];
            for (int i = 0; i < l; i++) {
                for (int j = 0; j < c; j++) {
                    this.tablier[i][j] = 'o';
                }
            }
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 2; j++) {
                    this.tablier[i][j] = ' ';
                }
            }
            for (int i = l-2; i < l; i++) {
                for (int j = 0; j < 2; j++) {
                    this.tablier[i][j] = ' ';
                }
            }
            for (int i = 0; i < 2; i++) {
                for (int j = c - 2; j < c; j++) {
                    this.tablier[i][j] = ' ';
                }
            }
            for (int i = l-2; i < l; i++) {
                for (int j = c - 2; j < c; j++) {
                    this.tablier[i][j] = ' ';
                }
            }
            this.tablier[3][3] = '.';
        }
        else {
            String s = new String();
            int l = 0, c = 0;
            String ligne;
            BufferedReader fich = new BufferedReader(new FileReader(emplacement));
            while ((ligne = fich.readLine()) != null) {
                c = ligne.length();
                s += ligne;
                l++;
            }
            this.tablier = new char[l][c];
            int k = 0;
            for (int i = 0; i < l; i++) {
                for (int j = 0; j < c; j++) {
                    this.tablier[i][j] = s.charAt(k);
                    k++;
                }
            }
        }
        this.nbBilleDepart = this.nbBille();
        this.liste_tablier = new ArrayList<char[][]>();
    }

    /**
     * Methode enleverUneBille qui retourne true si la bille a ete enlevee
     *
     * @param i
     *      Indice de la colonne du tableau
     * @param j
     *      Indice de la ligne du tableau
     *
     * @return true si la bille a ete enlevee
     */
    public boolean eneleverUneBille(int i,int j){
        if (this.tablier[i][j] == 'o') {
            this.tablier[i][j] = '.';
            return true;
        }
        return false;
    }

    /**
     * Methode tailleX qui retourne le nombre de colonnes du tableau
     *
     * @return le nombre de colonnes du tableau
     */
    public int tailleX(){
        return this.tablier.length;
    }

    /**
     * Methode tailleY qui retourne le nombre de lignes du tableau
     *
     * @return le nombre de lignes du tableau
     */
    public int tailleY(){
        return this.tablier[0].length;
    }

    /**
     * Methode caseJeu qui retourne true si la case fait partie du tablier
     *
     * @param i
     *      Indice de la colonne du tableau
     * @param j
     *      Indice de la ligne du tableau
     *
     * @return true si la case fait partie du tablier
     */
    public boolean caseJeu(int i, int j){
        if(this.tablier[i][j] != ' '){
            return true;
        }
        return false;
    }

    /**
     * Methode caseLibre qui retourne true si la case est libre
     *
     * @param i
     *      Indice de la colonne du tableau
     * @param j
     *      Indice de la ligne du tableau
     *
     * @return true si la case est libre
     */
    public boolean caseLibre(int i, int j){
        if(this.tablier[i][j] == '.'){
            return true;
        }
        return false;
    }

    /**
     * Methode caseLibre qui retourne true si la case contient une bille
     *
     * @param i
     *      Indice de la colonne du tableau
     * @param j
     *      Indice de la ligne du tableau
     *
     * @return true si la case contient une bille
     */
    public boolean caseBille(int i, int j){
        if (i >= 0 && i < this.tailleX() && j >= 0 && j < this.tailleY()) {
            if (this.tablier[i][j] == 'o') {
                return true;
            }
        }
        return false;
    }

    /**
     * Methode deplacementBille qui retourne true si le deplacement d'une bille a ete effectue
     *
     * @param iDep
     *      Indice de la colonne de depart du tableau
     * @param jDep
     *      Indice de la ligne de depart du tableau
     * @param iArr
     *      Indice de la colonne d'arrivee du tableau
     * @param jArr
     *      Indice de la ligne d'arrivee du tableau
     *
     * @return true si le deplacement d'une bille a ete effectue
     */
    public boolean deplacementBille(int iDep, int jDep, int iArr, int jArr) {
        if (iDep >= 0 && iDep < this.tailleX() && jDep >= 0 && jDep < this.tailleY() && iArr >= 0 && iArr < this.tailleX() && jArr >= 0 && jArr < this.tailleY()){
            if (Math.abs(iDep - iArr) == 2 && Math.abs(jDep - jArr) == 0) {
                this.liste_tablier.add(this.copieTablier());
                this.tablier[iDep][jDep] = '.';
                if (iDep < iArr) {
                    this.tablier[iDep + 1][jDep] = '.';
                } else {
                    this.tablier[iDep - 1][jDep] = '.';
                }
                this.tablier[iArr][jArr] = 'o';
                return true;
            }
            if (Math.abs(iDep - iArr) == 0 && Math.abs(jDep - jArr) == 2) {
                this.liste_tablier.add( this.copieTablier());
                this.tablier[iDep][jDep] = '.';
                if (jDep < jArr) {
                    this.tablier[iDep][jDep + 1] = '.';
                } else {
                    this.tablier[iDep][jDep - 1] = '.';
                }
                this.tablier[iArr][jArr] = 'o';
                return true;

            }
        }
        return false;
    }

    /**
     * Methode annulation qui permet d'annuler un deplacement
     *
     * @param iDep
     *      Indice de la colonne de depart du tableau
     * @param jDep
     *      Indice de la ligne de depart du tableau
     * @param iArr
     *      Indice de la colonne d'arrivee du tableau
     * @param jArr
     *      Indice de la ligne d'arrivee du tableau
     */
    public void annulation(int iDep, int jDep, int iArr, int jArr){
        if(Math.abs(iDep-iArr) == 2 && Math.abs(jDep-jArr) == 0){
            this.liste_tablier.remove(this.liste_tablier.size() -1);
            this.tablier[iDep][jDep] = 'o';
            if(iDep<iArr){
                this.tablier[iDep+1][jDep] = 'o';
            }
            else{
                this.tablier[iDep-1][jDep] = 'o';
            }
            this.tablier[iArr][jArr] = '.';
        }
        if(Math.abs(iDep-iArr) == 0 && Math.abs(jDep-jArr) == 2){
            this.liste_tablier.remove(this.liste_tablier.size() -1);
            this.tablier[iDep][jDep] = 'o';
            if(jDep<jArr){
                this.tablier[iDep][jDep+1] = 'o';
            }
            else{
                this.tablier[iDep][jDep-1] = 'o';
            }
            this.tablier[iArr][jArr] = '.';
        }
    }

    /**
     * Methode nbBille qui retourne le nombre de billes contenues dans le tablier
     *
     * @return le nombre de billes contenues dans le tablier
     */
    public int nbBille(){
        int a = 0;
        for(int i=0; i<this.tailleX();i++){
            for(int j=0; j<this.tailleY();j++){
                if(this.tablier[i][j] == 'o'){
                    a++;
                }
            }
        }
        return a;
    }

    /**
     * Methode copieTablier qui retourne une copie du tablier
     *
     * @return une copie du tablier
     */
    private char[][] copieTablier(){
        char[][] tab = new char[this.tailleX()][this.tailleY()];
        for(int i=0;i<this.tailleX();i++){
            for(int j=0;j<this.tailleY(); j++){
                tab[i][j] = this.tablier[i][j];
            }
        }
        return tab;
    }

    public char[][] getTablier(){
        return this.tablier;
    }

    /**
     * Methode toString qui retourne une chaîne de caracteres permettant d'afficher le tablier
     *
     * @return une chaîne de caracteres permettant d'afficher le tablier
     */
    public String toString(){
        String n = new String();
        int k = 0;
        for (char[][] m : this.liste_tablier) {
            if(k != 0) {
                n += "\nEtape " + k + ":\n";
                for (int i = 0; i < this.tailleX(); i++) {
                    for (int j = 0; j < this.tailleY(); j++) {
                        n += m[i][j];
                    }
                    n += "\n";
                }
            }
            k++;
        }
        n+= "\nEtape finale : \n";
        for(int i=0;i<this.tailleX();i++){
            for(int j=0;j<this.tailleY(); j++){
                n +=this.tablier[i][j];
            }
            n+="\n";
        }

        return n;
    }

}